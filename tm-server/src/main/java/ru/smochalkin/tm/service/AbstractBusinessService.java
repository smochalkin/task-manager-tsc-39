package ru.smochalkin.tm.service;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.smochalkin.tm.api.IBusinessRepository;
import ru.smochalkin.tm.api.IBusinessService;
import ru.smochalkin.tm.api.service.IConnectionService;
import ru.smochalkin.tm.enumerated.Status;
import ru.smochalkin.tm.model.AbstractBusinessEntity;

import java.sql.Connection;
import java.util.List;

public abstract class AbstractBusinessService<E extends AbstractBusinessEntity> extends AbstractService<E> implements IBusinessService<E> {

    public AbstractBusinessService(@NotNull IConnectionService connectionService) {
        super(connectionService);
    }

}
