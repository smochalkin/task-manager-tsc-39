package ru.smochalkin.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.smochalkin.tm.api.IBusinessRepository;
import ru.smochalkin.tm.api.IBusinessService;
import ru.smochalkin.tm.enumerated.Status;
import ru.smochalkin.tm.exception.empty.EmptyIdException;
import ru.smochalkin.tm.exception.empty.EmptyNameException;
import ru.smochalkin.tm.exception.system.IndexIncorrectException;
import ru.smochalkin.tm.model.AbstractBusinessEntity;

import java.util.Comparator;
import java.util.List;

import static ru.smochalkin.tm.util.ValidateUtil.isEmpty;

public abstract class AbstractBusinessService<E extends AbstractBusinessEntity> extends AbstractService<E> implements IBusinessService<E> {

    @NotNull
    protected final IBusinessRepository<E> businessRepository;

    public AbstractBusinessService(@NotNull final IBusinessRepository<E> businessRepository) {
        super(businessRepository);
        this.businessRepository = businessRepository;
    }

    @Override
    public void clear(final String userId) {
        if (isEmpty(userId)) throw new EmptyIdException();
        businessRepository.clear(userId);
    }

    @Override
    @NotNull
    public List<E> findAll(@NotNull final String userId) {
        return businessRepository.findAll(userId);
    }

    @Override
    @NotNull
    public List<E> findAll(@NotNull final String userId, @NotNull final Comparator<E> comparator) {
        return businessRepository.findAll(userId, comparator);
    }

    @Override
    @Nullable
    public E findByName(@NotNull final String userId, @Nullable final String name) {
        if (isEmpty(name)) throw new EmptyNameException();
        return businessRepository.findByName(userId, name);
    }

    @Override
    @NotNull
    public E findByIndex(@NotNull final String userId, @NotNull final Integer index) {
        if (isNotIndex(userId, index)) throw new IndexIncorrectException();
        return businessRepository.findByIndex(userId, index);
    }

    @Override
    public void removeByName(@NotNull final String userId, @Nullable final String name) {
        if (isEmpty(name)) throw new EmptyNameException();
        businessRepository.removeByName(userId, name);
    }

    @Override
    public void removeByIndex(@NotNull final String userId, @NotNull final Integer index) {
        if (isNotIndex(userId, index)) throw new IndexIncorrectException();
        businessRepository.removeByIndex(userId, index);
    }

    @Override
    public void updateById(@Nullable final String id, @Nullable final String name, @Nullable final String desc) {
        if (isEmpty(id)) throw new EmptyIdException();
        if (isEmpty(name)) throw new EmptyNameException();
        businessRepository.updateById(id, name, desc);
    }

    @Override
    public void updateByIndex(@NotNull final String userId,
                              @NotNull final Integer index,
                              @Nullable final String name,
                              @Nullable final String desc) {
        if (isNotIndex(userId, index)) throw new IndexIncorrectException();
        if (isEmpty(name)) throw new EmptyNameException();
        businessRepository.updateByIndex(userId, index, name, desc);
    }

    @Override
    public void updateStatusById(@Nullable final String id, @NotNull final Status status) {
        if (isEmpty(id)) throw new EmptyIdException();
        businessRepository.updateStatusById(id, status);
    }

    @Override
    public void updateStatusByName(@NotNull final String userId,
                                   @Nullable final String name,
                                   @NotNull final Status status) {
        if (isEmpty(name)) throw new EmptyNameException();
        businessRepository.updateStatusByName(userId, name, status);
    }

    @Override
    public void updateStatusByIndex(@NotNull final String userId,
                                    @NotNull final Integer index,
                                    @NotNull final Status status) {
        if (isNotIndex(userId, index)) throw new IndexIncorrectException();
        businessRepository.updateStatusByIndex(userId, index, status);
    }

    @Override
    public boolean isNotIndex(@NotNull final String userId, @Nullable final Integer index) {
        if (index == null || index < 0) return true;
        return index >= businessRepository.getCountByUser(userId);
    }

}
