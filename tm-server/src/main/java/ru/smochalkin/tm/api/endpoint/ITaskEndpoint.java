package ru.smochalkin.tm.api.endpoint;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.smochalkin.tm.dto.Result;
import ru.smochalkin.tm.model.Session;
import ru.smochalkin.tm.model.Task;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import java.util.List;

public interface ITaskEndpoint {
    @WebMethod
    @SneakyThrows
    Result createTask(
            @WebParam(name = "session") @NotNull Session session,
            @WebParam(name = "name") @NotNull String name,
            @WebParam(name = "description") @NotNull String description
    );

    @WebMethod
    @SneakyThrows
    Result changeTaskStatusById(
            @WebParam(name = "session") @NotNull Session session,
            @WebParam(name = "id") @NotNull String id,
            @WebParam(name = "status") @NotNull String status
    );

    @WebMethod
    @SneakyThrows
    Result changeTaskStatusByIndex(
            @WebParam(name = "session") @NotNull Session session,
            @WebParam(name = "index") @NotNull Integer index,
            @WebParam(name = "status") @NotNull String status
    );

    @WebMethod
    @SneakyThrows
    Result changeTaskStatusByName(
            @WebParam(name = "session") @NotNull Session session,
            @WebParam(name = "name") @NotNull String name,
            @WebParam(name = "status") @NotNull String status
    );

    @WebMethod
    @SneakyThrows
    List<Task> findTaskAllSorted(
            @WebParam(name = "session") @NotNull Session session,
            @WebParam(name = "sort") @NotNull String strSort
    );

    @WebMethod
    @SneakyThrows
    List<Task> findTaskAll(
            @WebParam(name = "session") @NotNull Session session
    );

    @WebMethod
    @SneakyThrows
    Task findTaskById(
            @WebParam(name = "session") @NotNull Session session,
            @WebParam(name = "id") @NotNull String id
    );

    @WebMethod
    @SneakyThrows
    Task findTaskByName(
            @WebParam(name = "session") @NotNull Session session,
            @WebParam(name = "name") @NotNull String name
    );

    @WebMethod
    @SneakyThrows
    Task findTaskByIndex(
            @WebParam(name = "session") @NotNull Session session,
            @WebParam(name = "index") @NotNull Integer index
    );

    @WebMethod
    @SneakyThrows
    Result removeTaskById(
            @WebParam(name = "session") @NotNull Session session,
            @WebParam(name = "id") @NotNull String id
    );

    @WebMethod
    @SneakyThrows
    Result removeTaskByName(
            @WebParam(name = "session") @NotNull Session session,
            @WebParam(name = "name") @NotNull String name
    );

    @WebMethod
    @SneakyThrows
    Result removeTaskByIndex(
            @WebParam(name = "session") @NotNull Session session,
            @WebParam(name = "index") @NotNull Integer index
    );

    @WebMethod
    @SneakyThrows
    Result clearTasks(
            @WebParam(name = "session") @NotNull Session session
    );

    @WebMethod
    @SneakyThrows
    Result updateTaskById(
            @WebParam(name = "session") @NotNull Session session,
            @WebParam(name = "id") @NotNull String id,
            @WebParam(name = "name") @NotNull String name,
            @WebParam(name = "description") @NotNull String description
    );

    @WebMethod
    @SneakyThrows
    Result updateTaskByIndex(
            @WebParam(name = "session") @NotNull Session session,
            @WebParam(name = "index") @NotNull Integer index,
            @WebParam(name = "name") @NotNull String name,
            @WebParam(name = "description") @NotNull String description
    );

    @WebMethod
    @SneakyThrows
    List<Task> findTasksByProjectId(
            @WebParam(name = "session") @NotNull Session session,
            @WebParam(name = "projectId") @NotNull String projectId
    );

    @WebMethod
    @SneakyThrows
    Result bindTaskByProjectId(
            @WebParam(name = "session") @NotNull Session session,
            @WebParam(name = "projectId") @NotNull String projectId,
            @WebParam(name = "taskId") @NotNull String taskId
    );

    @WebMethod
    @SneakyThrows
    Result unbindTaskByProjectId(
            @WebParam(name = "session") @NotNull Session session,
            @WebParam(name = "projectId") @NotNull String projectId,
            @WebParam(name = "taskId") @NotNull String taskId
    );
}
