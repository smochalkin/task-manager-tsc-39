package ru.smochalkin.tm.service;

import org.jetbrains.annotations.NotNull;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import ru.smochalkin.tm.api.service.*;
import ru.smochalkin.tm.enumerated.Status;
import ru.smochalkin.tm.exception.entity.EntityNotFoundException;
import ru.smochalkin.tm.model.Project;

import java.util.List;
import java.util.UUID;

public class ProjectServiceTest {

    @NotNull
    private IProjectService projectService;

    private static final String USER_ID_1 = UUID.randomUUID().toString();

    private static final String USER_ID_2 = UUID.randomUUID().toString();

    private int projectCount;

    @Before
    public void init() {
        @NotNull final IPropertyService propertyService = new PropertyService();
        @NotNull final IConnectionService connectionService = new ConnectionService(propertyService);
        projectService = new ProjectService(connectionService);
        projectService.create(USER_ID_1, "test1", "test1");
        projectService.create(USER_ID_2, "test2", "test2");
        projectCount = projectService.getCount();
    }

    @After
    public void end() {
        projectService.removeByName(USER_ID_1,"test1");
        projectService.removeByName(USER_ID_2,"test2");
        projectService.removeByName(USER_ID_1,"test3");
        projectService.removeByName(USER_ID_1,"new name");
    }

    @Test
    public void createTest() {
        projectService.create(USER_ID_1, "test3", "test3");
        Assert.assertEquals(projectCount + 1, projectService.getCount());
    }


    @Test
    public void findAllTest() {
        @NotNull List<Project> projectList = projectService.findAll(USER_ID_1);
        Assert.assertEquals(1, projectList.size());
    }

    @Test
    public void findByIdTest() {
        @NotNull final String projectId = projectService.findByName(USER_ID_1,"test1").getId();
        Assert.assertEquals(projectId, projectService.findById(USER_ID_1, projectId).getId());
    }

    @Test
    public void findByNameTest() {
        Assert.assertEquals("test1", projectService.findByName(USER_ID_1,"test1").getName());
    }

    @Test
    public void findByIndexTest() {
        Assert.assertEquals("test1", projectService.findByIndex(USER_ID_1,0).getName());
    }

    @Test
    public void removeByIdTest() {
        @NotNull final String projectId = projectService.findByName(USER_ID_1,"test1").getId();
        projectService.removeById(USER_ID_1, projectId);
        Assert.assertNull(projectService.findById(USER_ID_1, projectId));
    }

    @Test
    public void removeByNameTest() {
        projectService.removeByName(USER_ID_1, "test1");
        Assert.assertNull(projectService.findByName(USER_ID_1, "test1"));
    }

    @Test
    public void removeByIndexTest() {
        projectService.removeByIndex(USER_ID_1, 0);
        Assert.assertNull(projectService.findByIndex(USER_ID_1, 0));
    }

    @Test
    public void updateByIdTest() {
        @NotNull final String projectId = projectService.findByName(USER_ID_1,"test1").getId();
        projectService.updateById(USER_ID_1, projectId, "new name", "new desc");
        @NotNull final Project project = projectService.findById(USER_ID_1, projectId);
        Assert.assertEquals("new name", project.getName());
        Assert.assertEquals("new desc", project.getDescription());
    }

    @Test
    public void updateByIndexTest() {
        projectService.updateByIndex(USER_ID_1, 0, "new name", "new desc");
        @NotNull final Project project = projectService.findByIndex(USER_ID_1, 0);
        Assert.assertEquals("new name", project.getName());
        Assert.assertEquals("new desc", project.getDescription());
    }

    @Test
    public void updateStatusByIdTest() {
        @NotNull final String projectId = projectService.findByName(USER_ID_1,"test1").getId();
        projectService.updateStatusById(USER_ID_1, projectId, "COMPLETED");
        @NotNull final Project project = projectService.findById(USER_ID_1, projectId);
        Assert.assertEquals(Status.COMPLETED,project.getStatus());
    }

    @Test
    public void updateStatusByNameTest() {
        projectService.updateStatusByName(USER_ID_1, "test1", "COMPLETED");
        @NotNull final Project project = projectService.findByName(USER_ID_1, "test1");
        Assert.assertEquals(Status.COMPLETED, project.getStatus());
    }

    @Test
    public void updateStatusByIndexTest() {
        projectService.updateStatusByIndex(USER_ID_1, 0, "COMPLETED");
        @NotNull final Project project = projectService.findByIndex(USER_ID_1, 0);
        Assert.assertEquals(Status.COMPLETED, project.getStatus());
    }

    @Test
    public void isNotIndex() {
        Assert.assertTrue(projectService.isNotIndex(USER_ID_1, 99));
        Assert.assertFalse(projectService.isNotIndex(USER_ID_1, 0));
    }

}
